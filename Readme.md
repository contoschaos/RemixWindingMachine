# Linear winding machine

Can be used for e.g. wound wire coils, spools of thread.

Additional information in ```Documents``` folder.

![Winder machine img](./winder.jpg)

![Closeup of machine](./closeup.jpg)
