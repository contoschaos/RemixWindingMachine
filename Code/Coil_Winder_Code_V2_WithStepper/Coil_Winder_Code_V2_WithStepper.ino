#include <Wire.h>

#include "config.h"
#include "lcdUtil.h"
#include "stepperUtil.h"
#include "buttonsUtil.h"

// Function to initialize the wire_step_divisor array
void initializeWireStepDivisors() {
  const float base_distance = 4.0; // mm per rotation for 3200:1 ratio

  for(byte i = 0, swg = MIN_SWG; swg <= MAX_SWG; swg++,i++) {
    if(SWG_SIZES[i] > 0.0) { // Ensure SWG size is defined
      float divisor = base_distance / SWG_SIZES[i];
      wire_step_divisor[i] = round(divisor); // Round to nearest integer
    } else {
      wire_step_divisor[i] = 1; // Default divisor
    }
    swg_names[i] = swg;
    Serial.print(i);
  }
}

void setup()
{
  pinMode(UP_BUTTON, INPUT_PULLUP);
  pinMode(DN_BUTTON, INPUT_PULLUP);
  pinMode(ENT_BUTTON, INPUT_PULLUP);

  pinMode(R_LIMIT, INPUT_PULLUP); // RIGHT LIMIT
  pinMode(L_LIMIT, INPUT_PULLUP); // LEFT LIMIT

  pinMode(ROTOR_DIR, OUTPUT);  // Rotor direction
  pinMode(ROTOR_STEP, OUTPUT); // Rotor Step

  pinMode(FOLLOW_DIR, OUTPUT);  // Follower direction
  pinMode(FOLLOW_STEP, OUTPUT); // Follower step

  pinMode(RST_PIN, OUTPUT);              // Follower & Rotor RST
  pinMode(DISENGAGE_ENABLE_ROT, OUTPUT); // Rotor enable
  pinMode(DISENGAGE_ENABLE_FOL, OUTPUT); // Rotor enable

  Serial.begin(250000);
  Serial.println(VERSION);
  
  initializeWireStepDivisors();

  initLCD();
  initSteppers();
}

void programStateBack()
{
  if (isArrowLeft())
    holdTimeSet();
  else
    holdReset();

  if (hold_duration > 3000)
  {
    program_state--;
    lcdPrint("Going back...","");
    delay(USER_DELAY * 10);
  }
}

void updateNumbTurns(bool add)
{
  int change = (add ? 1 : -1);
  if (hold_duration > 5000)
  {
    change = change * 50;
  }
  else if (hold_duration > 2000)
  {
    change = change * 10;
  }

  n_turns = n_turns + change;

  if (n_turns < 1)
  {
    n_turns = 1;
  }
  else if (n_turns > 9999)
  {
    n_turns = 9999;
  }

  holdTimeSet();

  char buffer[16];
  formatTurnsBuffer(buffer);
  clearRow(1,buffer);
}

void selectCount()
{
  while (isEnter())
    handleArrowPress(updateNumbTurns);

  if(layer_turns > n_turns)
    layer_turns = n_turns;
  move_s_last = move_s;
}

void updateLength(bool add)
{
  int change = (add ? 1 : -1);
  if (hold_duration > 5000)
  {
    change = change * 50;
  }
  else if (hold_duration > 2000)
  {
    change = change * 10;
  }

  layer_turns = layer_turns + change;

  if (layer_turns < 1)
  {
    layer_turns = 1;
  }
  else if (layer_turns >= n_turns)
  {
    layer_turns = n_turns;
  }

  holdTimeSet();

  char buffer[16];
  dtostrf(SWG_SIZES[wire_gage_index] * layer_turns,0,2,buffer);
  clearRow(1,buffer);
}

void selectLength()
{
  while (isEnter())
    handleArrowPress(updateLength);

  move_s_last = move_s;
}



void selectAwg()
{
  while (isEnter())
  {
    checkArrowsState();


    // Check max index
    if (isArrowRight() && wire_gage_index < MAX_SWG-MIN_SWG)
    {
      wire_gage_index++;
      clearRow(1,String(swg_names[wire_gage_index]));
    }
    else if (isArrowLeft() && wire_gage_index > 0)
    {
      wire_gage_index--;
      clearRow(1,String(swg_names[wire_gage_index]));
    }
    delay(USER_DELAY);
  }

  move_s_last = move_s;
}

void manualMove()
{
  while (isEnter())
  {
    checkArrowsState();

    if(arrowsNotPressed() || canMove())
      {
        move_s_last = move_s;
        delay(USER_DELAY);
        continue;
      }

    if(!arrowsNotPressed() && move_s != move_s_last)
      digitalWrite(FOLLOW_DIR, isArrowRight() ? HIGH : LOW);

    enableAll();
    for (int i = 0; i < ONE_ROTATION_STEPS; i++)
    {
      checkLimitersState();

      if (canMove())
        break;
      
      followDoStep();
    }
    disableAll();

    move_s_last = move_s;
  }
  
  followRightDir();
}

void winding()
{
  bool right_dir = true;
  followRightDir();

  enableAll();

  while (isEnter() && total_counter < n_turns)
  {
    if(follower_length_index >= layer_turns){
        digitalWrite(FOLLOW_DIR, right_dir ? LOW : HIGH);
        right_dir = !right_dir;
        follower_length_index=0;
        Serial.println("Next layer!");
    }

    // One rotation of NEMA 17 needs multiple steps
    for (int i = 0; i < ONE_ROTATION_STEPS; i++, follower_steps_index++){
      checkLimitersState();

      if (limiter_sw_state == LIMIT_LEFT){
        followRightDir();
        right_dir = true;
        //Serial.println("Hit left limit!");
      }
      else if (limiter_sw_state == LIMIT_RIGHT){
        followLeftDir();
        right_dir = false;
        //Serial.println("Hit right limit!");
      }

      if (follower_steps_index >= wire_step_divisor[wire_gage_index]){
        allDoStep();
        follower_steps_index = 0;
      }
      else
        rotorDoStep();
    }
    total_counter++;
    follower_length_index++;
    //Serial.print("At: " + String(total_counter) + " / " + String(n_turns) + ";\r");
    //char buffer[16];
    //snprintf(buffer, sizeof(buffer), "At:%d/%d", total_counter,n_turns);
    //lcdPrint("Winding", buffer);
  }

  total_counter = 0;
  follower_length_index = 0;

  disableAll();
}

void nextState()
{
  delay(USER_DELAY * 3);
  program_state++;
}

void formatTurnsBuffer(char *buffer) {
    int total_seconds = n_turns * ROTOR_TURN_TIME_S;

    int minutes = total_seconds / 60;
    byte seconds = total_seconds % 60;

    // Format the buffer to 16 characters with the specified layout
    snprintf(buffer, 16, "%4d | %4dm%02ds", n_turns, minutes, seconds);
}

void loop()
{
  enter_s = 1 - isEnter();
  checkArrowsState();

  switch (program_state)
  {
  ///==========================================STATE 0: HOME FOLLOWER
  case 0:
  {
    Serial.println("Homing");
    lcdPrint("Moving home ...", "(Enter to esc)");

    followerHome();

    nextState();
    break;
  }
  ///==========================================STATE 1: AWG SELECTION
  case 1:
  {
    
    Serial.println("Select SWG");
    char buffer[16];
    snprintf(buffer, sizeof(buffer), "%d", swg_names[wire_gage_index]);
    lcdPrint("Wire SWG Gauge:", buffer);

    selectAwg();

    nextState();
    break;
  }

  ///==========================================STATE 2: TURN COUNT SELECTION
  case 2:
  {
    Serial.println("Select turn Count");
    char buffer[16];
    formatTurnsBuffer(buffer);
    lcdPrint("Number of turns:", buffer);

    selectCount();

    nextState();
    break;
  }
  ///==========================================STATE 3: LENGTH SELECTION
  case 3:
  {
    Serial.println("Select length");

    char buffer[16];
    dtostrf(SWG_SIZES[wire_gage_index] * layer_turns,0,2,buffer);
    lcdPrint("Length in mm:", buffer);

    selectLength();

    nextState();
    break;
  }
  ///==========================================STATE 4: VERIFICATION
  case 4:
  {
    Serial.println("Verification");

    char buffer[3][16];
    char secondBuffer[16];
    dtostrf(SWG_SIZES[wire_gage_index] * layer_turns,0,2,buffer[1]);
    snprintf(buffer[0], sizeof(buffer[0]), "%dTurns|%dSWG", n_turns, swg_names[wire_gage_index]);
    snprintf(buffer[2], sizeof(buffer[2]), "%smm | OK?", buffer[1]);
    lcdPrint(buffer[0], buffer[2]);

    while (isEnter())
    {
      delay(USER_DELAY);
    }

    nextState();
    move_s_last = move_s;
    break;
  }
  ///==========================================STATE 5: MANUAL MOVE BEFORE START
  case 5:
  {
    Serial.println("Manual move start");
    lcdPrint("Move head, use", "< and > , |->");

    manualMove();

    nextState();
    move_s_last = move_s;
    break;
  }
  ///==========================================STATE 6: Automated winding
  case 6:
  {
    lcdPrint("Winding", "Starting coil...");
    delay(USER_DELAY * 10);
    lcdPrint("Winding in", "progress...");

    winding();

    char buffer[16];
    snprintf(buffer, sizeof(buffer), "%d Turns", n_turns);
    lcdPrint(buffer, "Complete");

    nextState();
    break;
  }
  ///==========================================STATE 7: Waiting for return to start
  case 7:
  {
    Serial.println("Done");

    while (isEnter())
    {
      delay(USER_DELAY);
    }

    delay(USER_DELAY * 3);
    program_state = 0;
    digitalWrite(FOLLOW_STEP, LOW);
    digitalWrite(ROTOR_STEP, LOW);

    break;
  }
  }
}
