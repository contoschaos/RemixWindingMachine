#include <LiquidCrystal_I2C.h>

#include "config.h"

LiquidCrystal_I2C lcd(0x27, LCD_ROW_SIZE, 2); // (Address, columns, rows)
char cleanString[LCD_ROW_SIZE + 1];

void initLCD(){
  lcd.init();
  lcd.backlight();
  for (int i = 0; i < LCD_ROW_SIZE; i++) {
    cleanString[i] = ' ';
  }
  cleanString[LCD_ROW_SIZE] = '\0';
}

void clearRow(int rowIndex,String buffer){
  lcd.setCursor(0, rowIndex);
  lcd.print(cleanString); //
  lcd.setCursor(0, rowIndex);
  lcd.print(buffer);
}

void lcdPrint(String firstLine, String secondLine)
{
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(firstLine);
  lcd.setCursor(0, 1);
  lcd.print(secondLine);
}