#ifndef CONFIG_H
#define CONFIG_H
// Define your constants and configuration options here
#define VERSION "1.0.0"

//Outputs
#define DISENGAGE_ENABLE_ROT 6
#define DISENGAGE_ENABLE_FOL 12

#define ROTOR_DIR 7
#define ROTOR_STEP 8

#define FOLLOW_DIR 10
#define FOLLOW_STEP 11

// Inputs
#define R_LIMIT 4
#define L_LIMIT 9
#define RST_PIN 13

#define UP_BUTTON 14
#define DN_BUTTON 15
#define ENT_BUTTON 16

#define ROTOR_TURN_TIME_S 1.35

// Update based on your driver and stepper 
// I use Sixteenth Step rotor and Eighth Step follower
#define STEP_MICRO_WAIT 100
// If follower rotate at same rate (3200) then have divisor 1,
// Then it moves 4mm
#define ONE_ROTATION_STEPS 3200

#define USER_DELAY 100

#define LCD_ROW_SIZE 16

inline int n_turns = 100;
inline int layer_turns = 100;

inline byte move_s = 0, move_s_last = 0;
inline byte enter_s = 0, enter_s_last = 0;
inline byte program_state = 0, program_state_last = 0;

inline byte limiter_sw_state = 0;
inline byte follower_direction = 0;
inline byte follower_steps_index = 0;
inline byte follower_length_index = 0;

inline long hold_start_time = 0;
inline int hold_duration = 0;

inline int total_counter = 0;
inline byte wire_gage_index = 0;

// Define the maximum SWG size
#define MIN_SWG 14
#define MAX_SWG 50

inline float SWG_SIZES[MAX_SWG + 1] = {
  1.628,  // SWG 14
  1.450,  // SWG 15
  1.291,  // SWG 16
  1.150,  // SWG 17
  1.024,  // SWG 18
  0.912,  // SWG 19
  0.812,  // SWG 20
  0.711,  // SWG 21
  0.635,  // SWG 22
  0.560,  // SWG 23
  0.500,  // SWG 24
  0.450,  // SWG 25
  0.400,  // SWG 26
  0.350,  // SWG 27
  0.300,  // SWG 28
  0.270,  // SWG 29
  0.240,  // SWG 30
  0.220,  // SWG 31
  0.200,  // SWG 32
  0.180,  // SWG 33
  0.160,  // SWG 34
  0.140,  // SWG 35
  0.125,  // SWG 36
  0.115,  // SWG 37
  0.105,  // SWG 38
  0.095,  // SWG 39
  0.085,  // SWG 40
  0.075,  // SWG 41
  0.065,  // SWG 42
  0.060,  // SWG 43
  0.055,  // SWG 44
  0.050,  // SWG 45
  0.045,  // SWG 46
  0.040,  // SWG 47
  0.035,  // SWG 48
  0.030,  // SWG 49
  0.027   // SWG 50
};

// Array to store wire_step_divisor for each SWG
inline byte wire_step_divisor[MAX_SWG+1-MIN_SWG] = {0};
inline byte swg_names[MAX_SWG+1-MIN_SWG] = {0};

#endif
