#include "config.h"

#define NONE 0

#define LIMIT_RIGHT 1
#define LIMIT_LEFT 2

#define ADD_RIGHT 1
#define SUBTRACT_LEFT 2

typedef void (*UpdateFunction)(bool);

void holdReset(){
  hold_start_time = 0;
  hold_duration = 0;
}

void checkLimitersState(){
  limiter_sw_state = (1 - digitalRead(R_LIMIT)) + (1 - digitalRead(L_LIMIT)) * 2;
}

checkArrowsState(){
  move_s = (1 - digitalRead(UP_BUTTON)) + (1 - digitalRead(DN_BUTTON)) * 2;
}

bool arrowsNotPressed(){
  return move_s == 0;
}

bool isEnter(){
  return digitalRead(ENT_BUTTON);
}

bool isArrowRight(){
  return move_s == ADD_RIGHT;
}

bool isArrowLeft(){
 return move_s == SUBTRACT_LEFT;
}

bool canMove(){
  return (limiter_sw_state == LIMIT_LEFT && isArrowLeft()) || (limiter_sw_state == LIMIT_RIGHT && isArrowRight());
}

void handleArrowPress(UpdateFunction updateFunc) {
  checkArrowsState();
  if (isArrowRight() && n_turns < 9999) {
    updateFunc(true);   // Add
  } else if (isArrowLeft() && n_turns > 0) {
    updateFunc(false);  // Subtract
  } else {
    holdReset();
  }
  move_s_last = move_s;
  delay(USER_DELAY);
}


void holdTimeSet()
{
  if (move_s_last != move_s)
  {
    hold_start_time = millis();
  }
  hold_duration = millis() - hold_start_time;
}
