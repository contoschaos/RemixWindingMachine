#include "config.h"

void disableAll(){
  digitalWrite(DISENGAGE_ENABLE_ROT, HIGH);
  digitalWrite(DISENGAGE_ENABLE_FOL, HIGH);
}

void initSteppers(){
  // Pulse the sleep / reset on the stepper driver (doesn't work without this on A4988 boards)
  digitalWrite(RST_PIN, LOW);
  delay(50);
  digitalWrite(RST_PIN, HIGH);

  disableAll();

  digitalWrite(FOLLOW_STEP, HIGH);
  digitalWrite(ROTOR_STEP, HIGH);
  digitalWrite(FOLLOW_DIR, HIGH);
  digitalWrite(ROTOR_DIR, HIGH);
}

void enableAll() {
  digitalWrite(DISENGAGE_ENABLE_ROT, LOW);
  digitalWrite(DISENGAGE_ENABLE_FOL, LOW);
}

void followRightDir(){
  digitalWrite(FOLLOW_DIR, HIGH);
}

void followLeftDir(){
  digitalWrite(FOLLOW_DIR, LOW);
}

void followStateEna(bool enabled){
  digitalWrite(DISENGAGE_ENABLE_FOL, enabled ? LOW : HIGH);
}

void followDoStep(){
  digitalWrite(FOLLOW_STEP, HIGH);
  delayMicroseconds(STEP_MICRO_WAIT);
  digitalWrite(FOLLOW_STEP, LOW);
  delayMicroseconds(STEP_MICRO_WAIT);
}

void rotorDoStep(){
  digitalWrite(ROTOR_STEP, HIGH);
  delayMicroseconds(STEP_MICRO_WAIT);
  digitalWrite(ROTOR_STEP, LOW);
  delayMicroseconds(STEP_MICRO_WAIT);
}

void allDoStep(){
  digitalWrite(FOLLOW_STEP, HIGH);
  digitalWrite(ROTOR_STEP, HIGH);
  delayMicroseconds(STEP_MICRO_WAIT);
  digitalWrite(FOLLOW_STEP, LOW);
  digitalWrite(ROTOR_STEP, LOW);
  delayMicroseconds(STEP_MICRO_WAIT);
}

void followerHome()
{
  followLeftDir();
  followStateEna(true);

  while (digitalRead(L_LIMIT) && digitalRead(ENT_BUTTON))
    followDoStep();

  followRightDir();
  followStateEna(false);
}